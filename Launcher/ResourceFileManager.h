#pragma once

class ResourceFileManager : public Awesomium::DataSource
{
private:
	void OnRequest(int request_id, const Awesomium::WebString& path);
public:
	ResourceFileManager();
};