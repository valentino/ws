#include "stdafx.h"
#include "Enum.h"

Enum::Enum(const std::wstring& name) {
	mTemplate = v8::ObjectTemplate::New();
	mName = name;
}

void Enum::onRegister(v8::Handle<v8::ObjectTemplate> templ) {
	v8::Isolate* isolate = v8::Isolate::GetCurrent();

	v8::Local<v8::String> name2;
	v8::String::NewFromTwoByte(isolate, (const uint16_t*)mName.c_str(), v8::NewStringType::kNormal, mName.length()).ToLocal(&name2);

	templ->Set(name2, mTemplate);
}