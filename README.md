# README #

WildStar Studio F2P is a tool that lets you explore the game assets of WildStar since F2P.

This project is a fork of "mugadr_m/wildstar-studio" and was created so that this tool could continue to work post F2P.

## What is this repository for? ##

* Learn the formats used in the game using the source code
* Explore files and content of the game

## How do I get set up? ##

### Binary build ###

1. Download latest .7z build from [Downloads](http://bitbucket.org/Celess/wildstar-studio-f2p/downloads)
2. Un-zip to a folder
3. Run WildstarStudio.exe

### Build from source ###

** Prerequisites **

* Visual Studio 2015

* Windows Vista or newer

* DirectX SDK installed

** Build **

1. Checkout the source code

2. Open WildstarStudio.sln

3. Choose build configuration (debug or release) and projects to build

4. Build the selected configuration

### Checking issues for the User Interface ###

If you face problems with the user interface and want to check where they come from or how they could be resolved please open the following link in your browser:
http://localhost:55378 **(WildStar Studio F2P has to be open!)**

It will open a view similar to the chrome developer tools and you can work with the JavaScript console, modify DOM elements and so on. If you have an UI issue and are able to find a solution using the development tools please attach that info to your issue.