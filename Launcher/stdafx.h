#pragma once

#include <WinSock2.h>
#include <Windows.h>

#include <iostream>
#include <iomanip>
#include <map>
#include <set>
#include <vector>
#include <memory>
#include <algorithm>
#include <functional>
#include <filesystem>
#include <list>
#include <chrono>
#include <thread>
#include <mutex>
#include <future>
#include <string>
#include <sstream>
#include <fstream>
#include <locale>
#include <codecvt>
#include <cwctype>
#include <cctype>
#include <regex>
#include <condition_variable>
#define CURL_STATICLIB
#include <curl/curl.h>

#include <windowsx.h>
#include <gl/GL.h>
#include <gl/glext.h>
#include <gl/GLU.h>
#include <gl/wglext.h>
#include <gdiplus.h>
#include <CommCtrl.h>

#include <unrar/dll.hpp>

#include <Awesomium/WebCore.h>
#include <Awesomium/WebView.h>
#include <Awesomium/BitmapSurface.h>
#include <Awesomium/STLHelpers.h>

typedef unsigned __int64 uint64;
typedef signed __int64 int64;
typedef unsigned __int32 uint32;
typedef signed __int32 int32;
typedef unsigned __int16 uint16;
typedef signed __int16 int16;
typedef unsigned __int8 uint8;
typedef signed __int8 int8;

#define SHARED_TYPE(T) typedef std::shared_ptr<T> T##Ptr
#define SHARED_FWD(T) class T; SHARED_TYPE(T)

#include "String.h"
