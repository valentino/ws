#pragma once

#include "TcpSocket.h"

enum class HttpHeader
{
	Host,
	ContentType,
};

class HttpRequest : public std::enable_shared_from_this<HttpRequest>
{
	friend size_t _internal_curl_write_callback(void*, size_t, size_t, void*);
	friend int _internal_curl_progress(void*, double, double, double, double);

	CURL* mCurl;
	std::condition_variable mRespEvent;
	std::wstring mResponse;
	bool mIsBinary;
	std::vector<uint8> mBinaryContent;
	std::function<void (double, double)> mProgressCallback;
	std::function<void (uint8*, uint32)> mRawCallback;

	void onDownloadProgress(double current, double total);
public:
	HttpRequest(const std::wstring& address);

	void setRawCallback(std::function<void(uint8*, uint32)> callback) { mRawCallback = callback; }

	std::wstring getResponseSync();
	const std::vector<uint8>& getResponseSyncBinary(std::function<void(double, double)> progress = std::function<void(double, double)>());
	void asyncGetResponse(std::function<void (std::wstring)> callback, std::function<void (double, double)> progress = std::function<void (double, double)>());
	void asyncGetResponseBinary(std::function<void(const std::vector<uint8>&)> callback, std::function<void(double, double)> progress = std::function<void(double, double)>());
};

SHARED_TYPE(HttpRequest);